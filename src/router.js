import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/QuienesSomos',
      name: 'quienesSomos',
      component: () => import(/* webpackChunkName: "quienesSomos" */ './views/QuienesSomos.vue')
    },
    {
      path: '/areas/:id',
      name: 'areas',
      component: () => import(/* webpackChunkName: "areas*/ './views/Areas.vue')
    },
    {
      path: '/code',
      name: 'code',
      component: () => import(/* webpackChunkName: "areas*/ './views/Code.vue')
    },
    {
      path: '/competividad',
      name: 'competividad',
      component: () => import(/* webpackChunkName: "areas*/ './views/Competividad.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },

  ]
})
